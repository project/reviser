Drupal.behaviors.reviser = function (context) {
  $('.reviser').click(function(){
    var id = $(this).attr('id');
    this.editor = new DS.Reviser({
      elm:this,
      imageDir  : Drupal.settings.reviser.imageDir,
      beforeSave:function(html){
        return html;
      },
      afterSave:function(html){
        $.ajax({
          type: "POST",
          url: Drupal.settings.basePath + "reviser/save/node/" + Drupal.settings.reviser[id],
          data: "content=" + html,
          success: function(msg){
            alert( msg );
          }
        });
      }
    });
  });
};

